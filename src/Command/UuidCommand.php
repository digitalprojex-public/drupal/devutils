<?php

namespace Drupal\devutils\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Drupal\Console\Core\Command\Command;

/**
 * Class DefaultCommand.
 *
 * Drupal\Console\Annotations\DrupalCommand (
 *     extension="devutils",
 *     extensionType="module"
 * )
 */
class UuidCommand extends Command {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('devutils:uuid')
      ->setDescription($this->trans('commands.devutils.uuid.description'))
      ->addArgument('entity-type', InputArgument::REQUIRED, 'Entity type to search')
      ->addArgument('filter', InputArgument::OPTIONAL, 'Filter to search');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $entityType = $input->getArgument('entity-type');
    $filter = $input->getArgument('filter');

    $entities = [];
    if ($entityType === 'menu_link_content') {
      $menu_content_storage = \Drupal::entityTypeManager()
        ->getStorage('menu_link_content');
      /** @var \Drupal\menu_link_content\MenuLinkContentInterface[] $menu_link_contents */
      $entities = $menu_content_storage->loadByProperties(['menu_name' => $filter]);

    }

    if ($entityType === 'block') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('block_content')
        ->loadByProperties(['type' => $filter]);
    }

    if ($entityType === 'node') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['type' => $filter]);
    }

    foreach ($entities as $entity) {
      $this->getIo()->info('- '. $entity->uuid());
    }

  }

}
