<?php

namespace Drupal\devutils\Command;

use Drush\Commands\DrushCommands;

/**
 * A drush command file.
 *
 * @package Drupal\devutils\Command
 */
class DrushDevutilsCommands extends DrushCommands {

  /**
   * Drush command that export uuid for entities.
   *
   * @param string $entityType
   *   Entity type to search, available [node, menu_link, block,media, file,term ].
   * @param string $filter
   *   Filter to search, all.
   *
   * @command devutils:uuid
   * @aliases devutils uuid
   * @usage devutils:uuid node page
   */
  public function devutils($entityType = 'node', $filter = 'all') {
    $entities = [];
    if ($entityType === 'menu_link') {
      $menu_content_storage = \Drupal::entityTypeManager()
        ->getStorage('menu_link_content');
      /** @var \Drupal\menu_link_content\MenuLinkContentInterface[] $menu_link_contents */
      $entities = $menu_content_storage->loadByProperties(['menu_name' => $filter]);
    }

    if ($entityType === 'block') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('block_content')
        ->loadByProperties(['type' => $filter]);
    }

    if ($entityType === 'node') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadByProperties(['type' => $filter]);
    }

    if ($entityType === 'media') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('media')
        ->loadByProperties(['bundle' => $filter]);
    }

    if ($entityType === 'file') {
      /** @var \Drupal\file\FileInterface[] $files */
      $entities = \Drupal::entityTypeManager()
        ->getStorage('file')
        ->loadMultiple();
    }

    if ($entityType === 'term') {
      $entities = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['vid' => $filter]);
    }

    foreach ($entities as $entity) {
      $this->output()->writeln('- ' . $entity->uuid());
    }
  }

  /**
   * Drush command that clear not used file.
   *
   *
   * @command devutils:clear-files
   * @aliases devutils clear-files
   * @usage devutils:clear-files
   */
  public function clearFiles() {
    /** @var \Drupal\file\FileInterface[] $files */
    $files = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->loadByProperties();

    foreach ($files as $file) {
      $listUsage = \Drupal::service('file.usage')->listUsage($file);
      if(count($listUsage) == 0){
        $file->delete();
        $this->output()->writeln('Delete file:' . $file->label());
      }
    }

  }

}